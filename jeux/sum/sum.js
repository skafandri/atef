$(function() {
    $('#start').click(function() {
        Game.init(2);
    });
    $('#square').on('click', '.boxnum', function() {
        Game.process($(this));
    });
})

var Game = {
    level: 1,
    dimension: 2,
    nextTotal: 0,
    size: 0,
    selectedTotal: 0,
    selectedList: {},
    init: function(dimension) {
        this.dimension = dimension;
        this.size = 110 * this.dimension;
        $('#level strong').text(this.level);
        this.resizeSquare();
        this.populateSquare();
        this.fillNextTotal();
    },
    resizeSquare: function() {
        $('#square').width(this.size);
        $('#square').height(this.size);
    },
    populateSquare: function() {
        $('#square').html('');
        var count = Math.pow(this.dimension, 2);
        for (i = 0; i < count; i++) {
            var boxNum = getBoxNum(i);
            $('#square').append(boxNum);
        }
    },
    getNextTotal: function() {
        var sum = 0;
        var remainingList = $('.boxnum:not(.disabled)');
        if (remainingList.length > 1) {
            var count = getRandomizer(1, Math.min(remainingList.length, this.dimension));
            for (i = 0; i < count; i++) {
                var max = remainingList.length - 1;
                while (1) {
                    var index = getRandomizer(0, max)
                    if (($.inArray(remainingList[index], remainingList) !== -1)) {
                        sum += parseInt($(remainingList[index]).attr('val'));
                        remainingList.splice(index, 1);
                        break;
                    }
                }
            }
        } else {
            sum = $('.boxnum:not(.disabled)').attr('val');
        }
        return sum;
    },
    fillNextTotal: function() {
        this.nextTotal = this.getNextTotal();
        $('#next-total').text(this.nextTotal);
    },
    process: function(object) {
        if ($(object).hasClass('disabled')) {
            $('#square').effect('shake');
        } else {
            $(object).toggleClass('selected');
            if ($(object).hasClass('selected')) {
                this.selectedList[$(object).attr('id')] = $(object);
                this.selectedTotal += parseInt($(object).attr('val'));
            } else {
                delete this.selectedList[$(object).attr('id')];
                this.selectedTotal -= parseInt($(object).attr('val'));
            }
            if (this.selectedTotal > this.nextTotal) {
                $('#square').effect('shake');
                $('.boxnum').removeClass('selected');
                this.selectedTotal = 0;
                this.selectedList = {};
            } else if (this.selectedTotal == this.nextTotal) {
                $.each(this.selectedList, function(key, val) {
                    $('#' + key).removeClass('selected');
                    $('#' + key).addClass('disabled');
                });
                this.selectedTotal = 0;
                this.selectedList = {};
                if (isWon()) {
                    this.dimension++;
                    this.increaseLevel();
                    this.init(this.dimension);
                } else {
                    this.fillNextTotal();
                }
            }
        }
    },
    increaseLevel: function() {
        this.level++;
        $('#level strong').text(this.level);
    }
};

function isWon() {
    return $('.boxnum:not(.disabled)').length === 0;
}

function getBoxNum(index) {
    var boxnumTemplate = $('#boxnum-template').clone();
    $(boxnumTemplate).attr('id', 'boxnum-' + index);
    $(boxnumTemplate).removeClass('template');
    $(boxnumTemplate).addClass('boxnum');
    var val = getRandomizer(1, 9);
    $(boxnumTemplate).attr('val', val);
    $(boxnumTemplate).find('p').text(val);
    return $(boxnumTemplate);
}

function getRandomizer(min, max) {
    return Math.floor(Math.random() * (1 + max - min)) + min;
}


